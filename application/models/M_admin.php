<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	//COMPLETE ALL FUNCTION IN HERE
	
	public function GetCustomer(){
		$this->db->select('*');
		$this->db->from('customer');
		$query = $this->db->get();
		return $query->result();
    }

    public function getBarang(){
        $this->db->select('*');
        $this->db->from('barang');
        $query = $this->db->get();
		return $query->result();
    }

    public function jmlCustomer(){
        $this->db->select('count(id) as total');
        $this->db->from('customer');
        $query = $this->db->get();
        return $query->result();
    }

    public function getIdCust(){
        $this->db->select('*');
		return $this->db->get('customer')->row();
    }

    public function updateCustomer($id,$object){

            $this->db->where('id',$id);
            $this->db->update('customer',$object);
    }

    public function updateBarang($id,$object){

        $this->db->where('id',$id);
        $this->db->update('barang',$object);
    }
    public function tambahBarang($data){
        $this->db->insert('barang',$data);
    }

    public function getCustomerDoang($id){
        $this->db->select("*");
		$this->db->from("customer");
		$this->db->where("id",$id);
		$sql = $this->db->get();    
		return $sql->row();
    }

    function cekAdmin($username,$password){		
        $this->db->where('username',$username);
		$this->db->where('password',$password);
		$query = $this->db->get('admin');
	
		return $query;
    }	

    public function dataAdmin($username){
		$this->db->select("*");
		$this->db->from("admin");
		$this->db->where("username",$username);
		return $this->db->get();
	}


    public function getBarangDoang($id){
        $this->db->select("*");
		$this->db->from("barang");
		$this->db->where("id",$id);
		$sql = $this->db->get();    
		return $sql->row();
    }

    public function hapusCustomer($id){
            $this->db->delete('customer', array('id' => $id));
    }

    public function hapusBarang($id){
        $this->db->delete('barang', array('id' => $id));
}
}

?>