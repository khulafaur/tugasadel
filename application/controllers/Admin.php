<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		// Load M_admin as parents in here.
		parent::__construct();
		$this->load->model('M_admin','ma');

		if($this->session->userdata('adminLogin') == FALSE ){
			echo '<script>alert("Silahkan Login Terlebih Dahulu!");</script>';
			redirect('loginAdmin','refresh'); }
	}

	public function login(){
		$this->load->view('admin/login');
	}



	public function logout() {
		$this->session->sess_destroy();
		redirect('loginAdmin','refresh');
    }

	public function updateCustomer(){
		$firstName = $this->input->post('firstName');
		$lastName = $this->input->post('lastName');
		$email = $this->input->post('email');
		$id = $this->input->post('id');
		// echo $id; die;

		// echo $email; die;

		$object = array(
				'firs_name' =>  $firstName,
				'last_name' => $lastName,
				'email' => $email
			);
			// print_r($object); die;

		$this->ma->updateCustomer($id,$object);
		
		print "<script type=\"text/javascript\">alert('Update Data Sukses!');</script>";
        redirect('admin','refresh');
	}

	public function updateBarang(){
		$namaBarang = $this->input->post('namaBarang');
		$harga = $this->input->post('harga');
		$stock = $this->input->post('stock');
		$id = $this->input->post('id');
		// echo $id; die;

		// echo $email; die;

		$object = array(
				'namaBarang' =>  $namaBarang,
				'harga' => $harga,
				'stock' => $stock
			);
			// print_r($object); die;

		$this->ma->updateBarang($id,$object);
		
		print "<script type=\"text/javascript\">alert('Update Data Sukses!');</script>";
        redirect('admin/lihatBarang','refresh');
	}

	public function tambahBarang(){
		$this->load->view('admin/tambahBarang');
	}

	public function prosesTambahBarang(){
		$namaBarang = $this->input->post('namaBarang');
		$harga = $this->input->post('harga');
		$stock = $this->input->post('stock');

		$data = array(
			'namaBarang' => $namaBarang,
			'harga' => $harga,
			'stock' => $stock
		);

		$this->ma->tambahBarang($data);
		redirect('admin/lihatbarang','refresh');
	}


	public function edit(){
		$id = $this->uri->segment(3);
		$data['dataCustomer'] = $this->ma->getCustomerDoang($id);
		// print_r($data); die;
		$this->load->view('admin/v_edit',$data);
	}

	public function editBarang(){
		$id = $this->uri->segment(3);
		$data['dataBarang'] = $this->ma->getBarangDoang($id);
		// print_r($data); die;
		$this->load->view('admin/editBarang',$data);
	}

	public function lihatBarang(){
		$data['dataBarang'] = $this->ma->getBarang();
		$this->load->view('admin/lihatBarang',$data);
	}

	// public $data = array(
	// 	"id_admin" => "-",
	// 	"nama_admin" => "-",
	// );

	public function index()
	{

			// $data['pendaftar'] = $this->m_pendaftar->select_by_id();
			// $this->template->admin('admin/pesertaselengkapnya',$data);
			$data['dataCustomer'] = $this->ma->GetCustomer();
			$data['jmlCustomer'] = $this->ma->jmlCustomer();
			//Load page_header and page_index from views

			$this->load->view('admin/index',$data);
		

		
		
	}

	public function hapusCustomer(){
		$id = $this->uri->segment(3);
		$this->ma->hapusCustomer($id);
		redirect('admin','refresh');
	}

	public function hapusBarang(){
		$id = $this->uri->segment(3);
		$this->ma->hapusBarang($id);
		redirect('admin/lihatBarang','refresh');
	}



	public function pengguna()
	{
		$data_mahasiswa = $this->M_web->Getmahasiswa_nim();
		$data_jurusan = $this->M_web->Getjurusan_nim();
		$this->load->view('page_header',['dataJ'=>$data_jurusan]);
		$this->load->view('page_mahasiswa',['data'=>$data_mahasiswa]);
	}

	public function jurusan()
	{
		$data_jurusan = $this->M_web->Getjurusan_nim();
		$this->load->view('page_header');
		$this->load->view('page_jurusan',['data'=>$data_jurusan]);
	}


	#lengkapi FUNCTION BERIKUT
	public function hapusmahasiswa($nim)
	{

		//Load function hapus_mahasiswa from M_web
		// make it to index.php/web/mahasiswa after delete complete

		$this->M_web->hapus_mahasiswa($nim);
		redirect('index.php/web/mahasiswa');

	}


	public function tambahcustomer()
	{
		
		// Create variabel and use it for input data to database.
		// Load tambah_mahasiswa($data) from M_web
		// Redirect to index.php/web/mahasiswa after add data.
		$data = [
			"First Name" => $this->input->post('First Name',true),
			"Last Name" => $this->input->post('Last Name',true),
			"Email" => $this->input->post('Email',true),
			"Password" => $this->input->post('Password',true),
		];
		$this->M_web->tambah_Customer($data);
		redirect('index.php');


	}

	public function editmahasiswa()
	{

		// Create variabel and use it for edit data from database.
		// Load edit_mahasiswa($nim,$data) from M_web
		// Redirect to index.php/web/mahasiswa after edit data.
		$nim =  $this->input->post('nim',true);
		$data = [
			"nama" => $this->input->post('nama',true),
			"kelas" => $this->input->post('kelas',true),
			"id_jurusan" => $this->input->post('jurusan',true),
		];
		// print_r($data);
		$this->M_web->edit_mahasiswa($nim,$data);
		redirect('index.php/web/mahasiswa');


	}


	#lengkapi FUNCTION BERIKUT UNTUK PAGE JURUSAN



	public function tambahjurusan()
	{

		// Create variabel and use it for add data from database.
		// Load tambah_jurusan($data) from M_web
		// Redirect to index.php/web/jurusan after add data.
		$data = [
			"nama_jurusan" => $this->input->post('njurusan',true),
			"fakultas" => $this->input->post('nfakultas',true),
			"akreditasi" => $this->input->post('akreditasi',true),
		];
		$this->M_web->tambah_jurusan($data);
		redirect('index.php/web/jurusan');


	

	}

	public function editjurusan()
	{

		// Create variabel and use it for add data from database.
		// Load edit_jurusan($id_jurusan,$data) from M_web
		// Redirect to index.php/web/jurusan after add data.
		$id_jurusan = $this->input->post('id_jurusan',true);
		$data = [
			"nama_jurusan" => $this->input->post('njurusan',true),
			"fakultas" => $this->input->post('nfakultas',true),
			"akreditasi" => $this->input->post('akreditasi',true),
		];
		
		$this->M_web->edit_jurusan($id_jurusan, $data);
		redirect('index.php/web/jurusan');


	
	}

	public function hapusjurusan($id_jurusan)
	{
		// Create variabel and use it for add data from database.
		// Load hapus_jurusan($id_jurusan) from M_web
		// Redirect to index.php/web/jurusan after add data.
		$this->M_web->hapus_jurusan($id_jurusan);
		redirect('index.php/web/jurusan');

	}
}
