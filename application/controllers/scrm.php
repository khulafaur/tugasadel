<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Scrm extends CI_Controller
{
	public function __construct(){
		parent::__construct();
        $this->load->model('M_logincustomer','mc');
	}

	public function index()
	{
		$this->load->view('v_index');
		$this->load->view('format/v_footer');
	}

	public function registrasi()
	{
		$this->load->view('v_registrasi');
		$this->load->view('format/v_footer');
	}

	public function ongkir()
	{
		$this->load->view('v_ongkir');
		$this->load->view('format/v_footer');
	}
	public function service()
	{
		$this->load->view('v_service');
		$this->load->view('format/v_footer');
	}

	public function short()
	{
		$this->load->view('v_short');
		$this->load->view('format/v_footer');
	}
	public function jaket()
	{
		$this->load->view('v_jaket');
		$this->load->view('format/v_footer');
	}
		public function tshirt()
	{
		$this->load->view('v_tshirt');
		$this->load->view('format/v_footer');
	}
		public function login()
	{
		$this->load->view('v_login');
	}
	
	public function prosesRegistrasi(){
		$email = $this->input->post('email');
		$firstName = $this->input->post('firstName');
		$lastName = $this->input->post('lastName');
		$pass = $this->input->post('pass');

		$this->mc->daftar($email,$pass,$firstName,$lastName);
			print "<script type=\"text/javascript\">alert('Sukses Daftar!');</script>";
            redirect('scrm/login','refresh');

	}

}
