<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logincustomer extends CI_Controller {

    public function __construct(){
		parent::__construct();
        $this->load->model('M_logincustomer','mc');
	}

	public function index()
	{
        $cookie = $this->input->cookie('logged');
        if(isset($cookie) || isset($_SESSION['successLogin'])){
            $this->loginPage();
        } else {
            $data['title'] = "Login";
            /*Load view header,Login and Footer from Landing folder.
                Header and Login have a parameter $data.
             */

            $this->load->view('admin/login/login', $data);
        }
    }


    public function prosesLogin(){
		$email = $this->input->post('email', TRUE);
		$pass = $this->input->post('pass', TRUE);

		$cek = $this->mc->cekAkun($email,$pass)->row(0,'array');
        $hasil = count($cek);

        if ($cek){
            $object= array(
                'id'=>$cek['id'],
                'email' =>$cek['email'],
                'password'=>$cek['password']
            );

            $this->session->set_userdata('customerLogin',$object);
			// echo print_r($_SESSION);	die;
			
            
            redirect('halamanutama');
        }else{
            print "<script type=\"text/javascript\">alert('Username atau Password anda Salah!');</script>";
            redirect('scrm/login','refresh');
        }
	}

    public function loginPage() {
        
        /*
        1. CREATE COOKIE NAME LOGGED
        2. $data['image'] = ('replace this text to function getImage() in Model/User.php')
        3.LOAD VIEW LOGGEDIN AND USE PARAMETER $DATA */

        $cookie = $this->input->cookie('logged');
        $data['image'] = $this->User->getImage();
        $this->load->view('loggedin', $data);

    }

    public function Signin() {
        $user = $this->M_logincustomer->findUser();
        redirect('v_index');
        // if($user != null){
                $this->session->set_userdata('successLogin', $user[0]['Useradmin']);
                redirect('v_index');
        // } else {
        //     $this->session->set_flashdata('falselogin','nodata');
        //     redirect('Admin');
        // }
    }

    public function Logout() {
		$this->session->sess_destroy();
		redirect('halamanutama','refresh');
    }
}