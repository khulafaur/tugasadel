<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loginAdmin extends CI_Controller {

	public function __construct()
	{
		// Load M_admin as parents in here.
		parent::__construct();
		$this->load->model('M_admin','ma');

	}

    public function index(){
        $this->load->view('admin/login');
    }

    public function prosesLogin(){
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);
		$cek = $this->ma->cekAdmin($username,$password)->row(0,'array');
        $hasil = count($cek);

        if ($cek){
            $object= array(
                'id'=>$cek['id'],
                'username' =>$cek['username'],
                'password'=>$cek['password']
            );

            $this->session->set_userdata('adminLogin',$object);
			// echo print_r($_SESSION);	die;
			
            
            redirect('admin');
			}else{
				print "<script type=\"text/javascript\">alert('Username atau Password anda Salah!');</script>";
		  redirect('admin/login','refresh');
			}
	}
}
?>